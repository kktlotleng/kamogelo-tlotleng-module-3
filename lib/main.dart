import 'package:flutter/material.dart';
import 'package:module3_assignment1/screens/dashboard_screen.dart';
import 'package:module3_assignment1/screens/registration_screen.dart';

void main() {
  runApp(const Main());
}

class Main extends StatelessWidget {
  const Main({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.indigo),
      home: const Registration(),
    );
  }
}
