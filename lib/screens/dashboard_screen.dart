import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:module3_assignment1/main.dart';
import 'package:module3_assignment1/screens/chatroom_screen.dart';
import 'package:module3_assignment1/screens/settings_screen.dart';
import 'package:module3_assignment1/screens/userprofile_screen.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text("Dashboard"),
            centerTitle: true,
            //Removes the back button
            automaticallyImplyLeading: false),
        body: Column(
          children: [
            const Center(
              child: Text(
                "Dashboard",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
            const Padding(
              padding: EdgeInsets.all(8.0),
              child: Center(child: Text("View your latest feeds here")),
            ),
            FloatingActionButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: ((context) => Chatroom())));
              },
              tooltip: "Open a new chat",
              child: const Icon(Icons.chat),
            )
          ],
        ));
  }
} // close class

