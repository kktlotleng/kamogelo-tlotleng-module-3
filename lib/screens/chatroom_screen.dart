import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Chatroom extends StatefulWidget {
  Chatroom({Key? key}) : super(key: key);

  @override
  State<Chatroom> createState() => _ChatroomState();
}

class _ChatroomState extends State<Chatroom> {
  int _selectedItem = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Chatroom"),
        centerTitle: true,
      ),
      body: Column(
        children: const [
          Center(
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                "Chatroom",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Center(
              child: Padding(
            padding: EdgeInsets.all(9.0),
            child: Text("Chat using the below chat box."),
          )),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: TextField(
                decoration: InputDecoration(hintText: "Type message...")),
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: ElevatedButton(onPressed: null, child: Text("Send")),
          ),
        ],
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //     currentIndex: _selectedItem,
      //     onTap: _onItemTapped,
      //     selectedItemColor: Colors.blue,
      //     unselectedItemColor: Colors.grey,
      //     items: const <BottomNavigationBarItem>[
      //       BottomNavigationBarItem(
      //           icon: Icon(Icons.refresh), label: "Refresh"),
      //       BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
      //       BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile"),
      //     ]),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedItem = index;
    });
  }
} // class


